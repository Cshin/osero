package osero;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class Home extends Application {
	public static Board board;
	public static Group rootc = new Group();
	public static int flag = 0;
	public static int flagjudge = 0;
	public static int win = 0;
	public static int com = 0;
	public static int comnormal=0;
	public static Label label=new Label();
	public static Label turn = new Label();
	@Override
    public void start(Stage primaryStage) throws Exception{
    	primaryStage.setTitle("OSERO");
    	//TOP
    	Label lb1 = new Label("DSE OSERO");
    	lb1.prefWidth(240);
    	HBox top = new HBox();
    	top.setAlignment(Pos.CENTER);
    	top.setSpacing(10);
    	top.setPadding(new Insets(5,5,5,5));
    	top.getChildren().addAll(lb1);
    	
    	//left
    	VBox leftbox = new VBox();
    	Button leftbtn[] = new Button[7];
    	leftbtn[0] = new Button("PASS");
    	leftbtn[0].setPrefWidth(60);
    	leftbtn[0].setOnAction(event -> pass());
    	leftbtn[1] = new Button("PLAYER");
    	leftbtn[1].setPrefWidth(60);
    	leftbtn[1].setOnAction(event -> Home.com=0);
    	leftbtn[2] = new Button("COMweek");
    	leftbtn[2].setPrefWidth(60);
    	leftbtn[2].setOnAction(event -> Home.com=1);
    	leftbtn[3] = new Button("COMnormal");
    	leftbtn[3].setPrefWidth(60);
    	leftbtn[3].setOnAction(event -> Home.comnormal=1);
    	
    	for (int i=4; i<7; i++)
    	{
    		leftbtn[i] = new Button(Integer.toString(i));
    		leftbtn[i].setPrefWidth(60);
    	    if(i==6)
    	    {
    	    	leftbtn[i] = new Button("EXIT");
    	    	leftbtn[i].setPrefWidth(60);
    	    	leftbtn[i].setOnAction(event -> Platform.exit());
    	    }
    	    else
    	    {
        		leftbtn[i] = new Button(Integer.toString(i));
        		leftbtn[i].setPrefWidth(60);
    	    }
    	}
    	turn.setText("���̃^�[��");
        label.setText("��:"+2+ "��:"+2);
        leftbox.getChildren().addAll(turn,label);
    	leftbox.setAlignment(Pos.CENTER);
    	leftbox.setSpacing(10);
    	leftbox.setPadding(new Insets(5,5,5,5));
    	leftbox.getChildren().addAll(leftbtn);
    	
    	
    	//right
  /*  	
        ImageView imageView = new ImageView();
        Image image = new Image(getClass().getResourceAsStream("image.jpg"));
        imageView.setImage(image);

        VBox rightbox = new VBox();
        rightbox.getChildren().add(imageView);
        //Scene scene = new Scene(rightbox, 600, 800);
   
   */
    	
    	Canvas canvas = new Canvas(500,500);
    	VBox rightbox = new VBox();
        rightbox.getChildren().addAll(canvas);           
        GraphicsContext  gc = canvas.getGraphicsContext2D();
        Image img = new Image(getClass().getResource("img.jpg").toExternalForm());
        gc.drawImage( img, 0, 0 );
        //Circle  c = new Circle(208,167,15);
        //c.setFill(Color.BLACK);
        //bottom
        HBox bottom = new HBox();
        bottom.getChildren().addAll(leftbox,rightbox);
        
        //root 
        VBox root1 = new VBox();
        root1.setAlignment((Pos.CENTER));
        root1.setPadding(new Insets(5,5,5,5));
        root1.setSpacing(3.0);

        root1.getChildren().addAll(top,bottom);
        //Circle c[][] = null;
        
        
        
        
        
       
       rootc.getChildren().addAll(root1);
    
       
       
       /*for(int x=0;x<9;x++)
        {
        	for(int y=0;y<9;y++)
        	{
        		
        		if(Board.boardMatrix[x][y]=="��")
        		{
        			// c[x][y]= new Circle(92+(x+2)*34,51+(y+2)*34,15);
        			 Circle c= new Circle(128+(x-1)*54,88+(y-1)*53,22);
        			c.setFill(Color.BLACK);
        			rootc.getChildren().add(c);
        		}
        		else if(Board.boardMatrix[x][y]=="��")
        		{
        			Circle c= new Circle(128+(x-1)*54,88+(y-1)*53,22);
         			c.setFill(Color.WHITE);
         			rootc.getChildren().add(c);
        		}
        		
        	}
        }
        	
       Circle  c1 = new Circle(210,167,15);
        c1.setFill(Color.BLACK);
        Circle  c2 = new Circle(244,167,15);
        c2.setFill(Color.WHITE);
        Circle c3 = new Circle(210,201,15);
        c3.setFill(Color.WHITE);
        Circle c4 = new Circle(244,201,15);
        c4.setFill(Color.BLACK);
        */
        
        
      //  Group root = new Group();
      //  root.getChildren().addAll(root1);
     /*   for(int m=0;m<9;m++)
        {
        	for(int n=0;n<9;n++)
        	{		
        			if(c[m][n]!=null)		
        			root.getChildren().addAll(c[m][n]);
        	}
        }
        
        
       */
       
        rewrite();
        Mouse mouse = new Mouse();
        rootc.setOnMouseClicked(event -> mouse.mouseClickedblack(event));
        
        
        primaryStage.setTitle("Osero ver.0.01");
        primaryStage.setScene(new Scene(rootc));
        primaryStage.show();
        
        
    }
	
	private Object pass() {
		if(Home.comnormal==1)
		{
			if(Home.flag==1)
			{
				Home.flag=0;
				win++;
				rewrite();
			}
			if(Home.flag==0)
			{
				Home.flag=1;
				win++;
				
				Home.rewrite();
				 Board.reboard();
				 Player.judgewhite(Board.boardMatrix);
				Com.maxweight(Board.boardMatrix);
				 Board.boardMatrix=Board.rewrite(Board.boardMatrix);
				 Home.rewrite();
				 //System.out.print(Board.boardjudge[i][j]);
				 Board.reboard();
				 Player.judgeblack(Board.boardMatrix);
				 Home.flag=0;
				System.out.print(Home.flag);
				return null;
			}
				
		}
		if(Home.flag==1)
		{
			Home.flag=0;
			win++;
			rewrite();
		}
			else 
				{
					win++;
					Home.flag =1;
					rewrite();
				}
		// TODO Auto-generated method stub
		return null;
	}

	public static void rewrite()
	{
		
    for(int x=0;x<9;x++)
    {
    	for(int y=0;y<9;y++)
    	{
    		
    		if(Board.boardMatrix[x][y]=="��")
    		{
    			// c[x][y]= new Circle(92+(x+2)*34,51+(y+2)*34,15);
    			 Circle c= new Circle(128+(x-1)*54,88+(y-1)*53,22);
    			c.setFill(Color.BLACK);
    			rootc.getChildren().add(c);
    		}
    		else if(Board.boardMatrix[x][y]=="��")
    		{
    			Circle c= new Circle(128+(x-1)*54,88+(y-1)*53,22);
     			c.setFill(Color.WHITE);
     			rootc.getChildren().add(c);
    		}
    		
    	}
    }
	    if(Home.flag==0)
	    {
	    	Player.judgeblack(board.boardMatrix);
	    }
	    if(Home.flag==1)
	    {
	    	Player.judgewhite(board.boardMatrix);
	    }
	}

    public static void main(String[] args) {
    	board = new Board();
    	board.initial();
    	board.reboard();
    	Player.judgeblack(Board.boardMatrix);
    	launch(args);
    	//mouse.mouseClicked(e);
    	//board = new Board();
        Player C = new Player();
        //board.initial();
//		for(;;)
//		{
		//board=C.black(board.boardMatrix);
		//board=B.rewrite(board);
		//board=C.white(boardMatrix);
		//board=B.rewrite(board);
//		}
        
        //mouse.addMouseListener(this);
        //mouse.init();
    }
}



